#Install
- bundle install

- rails db:seed # some test data

# Filter Usage

- include CustomFilter in your model

- full_text - array of fields for full-text search

    - date_field - field for filtering by date
    
    - scopes: [{assoc: '', fields: []}] - list of association models for which filters are needed
        - assoc - model name
        
        - fields - fields for filter