module CustomFilter
  extend ActiveSupport::Concern

  module ClassMethods
    private

    def filterable(opts={})
      @filterable_options = opts

      create_date_filter
      create_scopes_filters
      create_full_text
    end

    def create_full_text
      include PgSearch
      pg_search_scope :search,
                      against: @filterable_options[:full_text],
                      using: {
                          tsearch: {
                              any_word: true,
                              prefix: true
                          }
                      }, order_within_rank: @filterable_options[:full_text].join(', ')
    end

    def create_scopes_filters
      @filterable_options[:scopes].each do |filter|
        filter[:fields].each do |field|
          define_singleton_method "filter_by_#{filter[:assoc]}_#{field}" do |arg|
            filter_body(filter[:assoc], field, arg)
          end
        end
      end
    end

    def create_date_filter
      define_singleton_method "filter_by_#{@filterable_options[:date_field]}" do |method, date|
        case method
          when :current
            where("#{date_field} BETWEEN ? AND ?", date.beginning_of_day, date.end_of_day)
          when :before
            where("#{date_field} < ?", date.beginning_of_day)
          when :after
            where("#{date_field} > ?", date.end_of_day)
          else
            raise ArgumentError, 'Unsupported method'
        end
      end
    end

    def filter_body(assoc, field, arg)
      joins(assoc).where("#{assoc.to_s.pluralize}": {"#{field}": arg})
    end

    def date_field
      "#{self.table_name}.#{@filterable_options[:date_field]}"
    end
  end
end