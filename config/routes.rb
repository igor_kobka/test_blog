Rails.application.routes.draw do
  post 'auth_user' => 'authentication#authenticate_user'

  resource :users, only: [:update, :destroy] do
    scope module: :users do
      resources :posts
    end
  end

  resources :users, only: [:index, :show, :create]

end
