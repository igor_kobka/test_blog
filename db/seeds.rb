# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


u = User.create(name: 'First user', email: 'a@a.com')

u.posts.create(name: 'Post 1',
               body: 'Lorem ips dolor sit amet, consectetur adipiscing elit.',
               published_at: Date.current)

u.posts.create(name: 'Post',
               body: 'Lorem ips dolor sit amet, consectetur adipiscing elit.',
               published_at: Date.current)

u.posts.create(name: 'Post 1',
               body: 'Etiam congue massa nec est finibus euismod',
               published_at: Date.current - 2.days)

u.posts.create(name: 'Post 1',
               body: 'Lorem ipsum. Sed sit amet nulla porttitor, ullamcorper purus quis, pharetra nunc. ',
               published_at: Date.current - 2.days)

u.posts.create(name: 'Post 1',
               body: 'Cras nec rutrum magna, consectetur adipiscing elit.',
               published_at: Date.current + 2.days)

u.posts.create(name: 'Post 1',
               body: 'Donec arcu ex, laoreet a maximus ac.',
               published_at: Date.current + 2.days)
