class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string :name, null: false
      t.text :body, null: false
      t.references :user, index: true, foreign_key: true, null: false
      t.datetime :published_at

      t.timestamps
    end

    add_index :posts, :name
    add_index :posts, :body
  end
end
