class CreateUserTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :user_tokens do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.string :token, null: false

      t.timestamps
    end
  end
end
