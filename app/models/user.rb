class User < ApplicationRecord
  has_many :posts, dependent: :destroy
  has_many :user_tokens, dependent: :destroy

  scope :over_n_posts, -> (n) {joins(:posts).group('users.id').having('count(user_id) > ?', n)}

  validates :name, presence: true
  validates :email, email: true

  def self.over_n_posts_published(n, method, date)
    over_n_posts(n).merge(Post.filter_by_published_at(method, date))
  end

end

