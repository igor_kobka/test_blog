class Post < ApplicationRecord
  include CustomFilter
  filterable full_text: [:name, :body],
             date_field: :published_at,
             scopes: [
                {
                    assoc: :user,
                    fields: [:id, :email]
                }
             ]

  belongs_to :user

  validates :name, :body, presence: true
end
