class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods

  rescue_from ActiveRecord::RecordInvalid do |exc|
    render json: exc.record.errors.as_json(full_messages: true), status: :unprocessable_entity
  end

  protected

    def authenticate_request!
      authenticate_or_request_with_http_token do |token, options|
        @current_user = UserToken.find_by(token: token).user
      end
    end

end
