class UsersController < ApplicationController
  before_action :authenticate_request!, except: [:index, :show, :create]

  def index
    render json: User.all
  end

  def show
    render json: User.find(params[:id])
  end

  def create
    user = User.create!(user_params)
    render json: user, status: :created
  end

  def update
    @current_user.update_attributes!(user_params)
    render json: @current_user, status: :ok
  end

  def destroy
    @current_user.destroy
  end

  private

    def user_params
      params.require(:user).permit(:name, :email)
    end
end
