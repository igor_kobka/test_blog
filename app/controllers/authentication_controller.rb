class AuthenticationController < ApplicationController
  def authenticate_user
    user = User.find_by(email: params[:email])
    if user.present?
      payload = payload(user)
      user.user_tokens.create(token: payload[:auth_token])

      render json: payload
    else
      render json: {errors: ['Invalid Email']}, status: :unauthorized
    end
  end

  private

  def payload(user)
    return nil unless user&.id
    {
        auth_token: JsonWebToken.encode({user_id: user.id}),
        user: {id: user.id, email: user.email}
    }
  end
end
