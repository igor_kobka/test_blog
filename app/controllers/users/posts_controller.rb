class Users::PostsController < ApplicationController
  before_action :authenticate_request!
  before_action :set_post, only: [:show, :update, :destroy]
  before_action :set_scope, only: [:index]

  def index
    @scope = @scope.search(params[:q]) if params[:q]
    render json: @scope
  end

  def show
    render json: @post
  end

  def create
    post = @current_user.posts.create!(post_params)
    render json: post, status: :created
  end

  def update
    @post.update_attributes!(post_params)
    render json: @post, status: :ok
  end

  def destroy
    @post.destroy
  end

  private

  def set_scope
    @scope = @current_user.posts
  end

  def post_params
    params.require(:post).permit(:name, :body)
  end

  def set_post
    @post = @current_user.posts.find(params[:id])
  end
end
