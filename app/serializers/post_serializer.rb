class PostSerializer < ActiveModel::Serializer
  attributes :id, :name, :body, :published_at
  belongs_to :user
end
